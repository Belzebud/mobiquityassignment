package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreatePage extends BasePage {

	public CreatePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	// buttons
	@FindBy(css = "#sub-nav > li > a")
	WebElement cancelButton;
	@FindBy(css = "body > div > header > div > p.main-button")
	WebElement logoutButton;
	@FindBy(css = "body > div > div > div > form > fieldset > div > button:nth-child(2)")
	WebElement addButton;

	// fields
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(3) > input")
	WebElement firstNameField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(4) > input")
	WebElement lastNameField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(5) > input")
	WebElement startDateField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(6) > input")
	WebElement emailField;

	public void pressCancelButton() {
		cancelButton.click();
	}

	public void pressLogoutButton() {
		logoutButton.click();
	}

	public void pressAddButton() {
		addButton.click();
	}

	public void inputFirstName(String firstName) {
		firstNameField.sendKeys(firstName);
	}

	public void inputLastName(String lastName) {
		lastNameField.sendKeys(lastName);
	}

	public void inputStartDate(String startDate) {
		startDateField.sendKeys(startDate);
	}

	public void inputEmail(String email) {
		emailField.sendKeys(email);
	}

	public void clearFirstName() {
		firstNameField.clear();
	}

	public void clearLastName() {
		lastNameField.clear();
	}

	public void clearStartDate() {
		startDateField.clear();
	}

	public void clearEmail() {
		emailField.clear();
	}

	public void addEmployee(String firstName, String lastName, String startDate, String email) {
		inputFirstName(firstName);
		inputLastName(lastName);
		inputStartDate(startDate);
		inputEmail(email);
	}
}
