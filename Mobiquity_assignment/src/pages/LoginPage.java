package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	// buttons
	@FindBy(css = "#login-form > fieldset > button")
	WebElement loginButton;

	// fields
	@FindBy(css = "#login-form > fieldset > label:nth-child(3) > input")
	WebElement usernameField;
	@FindBy(css = "#login-form > fieldset > label:nth-child(4) > input")
	WebElement passwordField;

	public void pressLoginButton() {
		loginButton.click();
	}

	public void inputUsername(String user) {
		usernameField.sendKeys(user);
	}

	public void inputPassword(String password) {
		passwordField.sendKeys(password);
	}

	public void loginWithUserAndPassword(String user, String password) {
		inputUsername(user);
		inputPassword(password);
		pressLoginButton();
	}
}
