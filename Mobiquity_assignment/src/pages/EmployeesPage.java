package pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeesPage extends BasePage {

	public EmployeesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	// buttons
	@FindBy(css = "body > div > header > div > p.main-button")
	WebElement logoutButton;
	@FindBy(css = "#bAdd")
	WebElement addButton;
	@FindBy(css = "#bEdit")
	WebElement editButton;
	@FindBy(css = "#bDelete")
	WebElement deleteButton;
	// other
	@FindBy(css = "#greetings")
	WebElement greetingsMessage;
	@FindBy(css = "#employee-list")
	WebElement employeesList;

	By employees = By.tagName("li");

	public void pressAddButton() {
		addButton.click();
	}

	public void pressEditButton() {
		editButton.click();
	}

	public void pressDeleteButton() {
		deleteButton.click();
	}

	public void pressLogoutButton() {
		logoutButton.click();
	}

	public boolean employeesListAvailable() {
		if (employeesList != null)
			return true;
		return false;
	}

	public String getGreetingsMessage() {
		return greetingsMessage.getText();
	}

	public boolean selectEmployee(String employeeName) {
		WebElement employee = findEmployee(employeeName);
		if (employee != null) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", employee);
			employee.click();
			return true;
		}
		return false;
	}

	public boolean doubleClickEmployee(String employeeName) {
		WebElement employee = findEmployee(employeeName);
		if (employee != null) {
			employee.click();
			employee.click();
			return true;
		}
		return false;
	}

	public boolean employeeExists(String employeeName) {
		if (findEmployee(employeeName) != null)
			return true;
		return false;
	}

	public void employeeDeleteConfirmation(boolean confirm) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		if (confirm) {
			alert.accept();
		} else {
			alert.dismiss();
		}
	}

	private WebElement findEmployee(String employeeName) {
		List<WebElement> availableEmployees = employeesList.findElements(employees);
		for (WebElement employee : availableEmployees) {
			String name = employee.getText();
			if (name.equalsIgnoreCase(employeeName)) {
				return employee;
			}
		}
		return null;
	}
}
