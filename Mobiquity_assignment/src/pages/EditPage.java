package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EditPage extends BasePage {

	public EditPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	// buttons
	@FindBy(css = "#sub-nav > li > a")
	WebElement backButton;
	@FindBy(css = "body > div > header > div > p.main-button")
	WebElement logoutButton;
	@FindBy(css = "body > div > div > div > form > fieldset > div > button:nth-child(1)")
	WebElement updateButton;
	@FindBy(css = "body > div > div > div > form > fieldset > div > p")
	WebElement deleteButton;

	// fields
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(3) > input")
	WebElement firstNameField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(4) > input")
	WebElement lastNameField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(5) > input")
	WebElement startDateField;
	@FindBy(css = "body > div > div > div > form > fieldset > label:nth-child(6) > input")
	WebElement emailField;

	public void pressBackButton() {
		backButton.click();
	}

	public void pressLogoutButton() {
		logoutButton.click();
	}

	public void pressUpdateButton() {
		updateButton.click();
	}

	public void inputFirstName(String firstName) {
		firstNameField.sendKeys(firstName);
	}

	public void inputLastName(String lastName) {
		lastNameField.sendKeys(lastName);
	}

	public void inputStartDate(String startDate) {
		startDateField.sendKeys(startDate);
	}

	public void inputEmail(String email) {
		emailField.sendKeys(email);
	}

	public void clearFirstName() {
		firstNameField.clear();
	}

	public void clearLastName() {
		lastNameField.clear();
	}

	public void clearStartDate() {
		startDateField.clear();
	}

	public void clearEmail() {
		emailField.clear();
	}
}
