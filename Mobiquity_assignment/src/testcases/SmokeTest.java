package testcases;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SmokeTest extends BaseTest {

	@Test(priority = 0)
	public void Login() {

		try {
			System.out.println("Step: Go to website");
			goToWebSite();
			Thread.sleep(500);
			System.out.println("Step: Login with user: " + userName + "and password: " + password);
			loginPage.loginWithUserAndPassword(userName, password);
			Thread.sleep(500);
			System.out.println("Step: Check if employeesList is available");
			Assert.assertTrue(employeesPage.employeesListAvailable(), "Employees List is not available after Login");
			System.out.println("Step: Check Greetings message");
			String greetingsMessage = employeesPage.getGreetingsMessage();
			Assert.assertTrue(greetingsMessage.toUpperCase().contains(userName.toUpperCase()),
					"Greetings message does not contain the user name");
		} catch (Exception ex) {
			System.out.println("Login test error " + ex.getMessage());
		}
	}

	@Test(priority = 1, dataProvider = "add-employees")
	public void CreateUser(String firstName, String lastName, String startDate, String email) {
		try {
			employeesPage.pressAddButton();
			Thread.sleep(500);
			createPage.addEmployee(firstName, lastName, startDate, email);
			Thread.sleep(500);
			createPage.pressAddButton();
			Thread.sleep(500);
			Assert.assertTrue(employeesPage.employeeExists(firstName + " " + lastName),
					"Added employee not found in Employees List");
		} catch (Exception ex) {
			System.out.println("Create User test error " + ex.getMessage());
		}
	}

	@Test(priority = 2, dataProvider = "add-employees")
	public void EditUser(String firstName, String lastName, String startDate, String email) {
		try {
			// select employee
			employeesPage.selectEmployee(firstName + " " + lastName);
			Thread.sleep(500);
			employeesPage.pressEditButton();
			Thread.sleep(500);

			// update employee data
			editPage.clearFirstName();
			editPage.inputFirstName(lastName);
			editPage.clearLastName();
			editPage.inputLastName(firstName);
			editPage.clearStartDate();
			editPage.inputStartDate(changeDate(startDate));
			editPage.clearEmail();
			editPage.inputEmail(changeEmail(email));
			Thread.sleep(500);
			editPage.pressUpdateButton();
			Thread.sleep(500);

			// check if user was changed
			Assert.assertTrue(employeesPage.employeeExists(lastName + " " + firstName),
					"Edited user not found in Employees List");
			Assert.assertFalse(employeesPage.employeeExists(firstName + " " + lastName),
					"Edited user still found with original name");
		} catch (Exception ex) {
			System.out.println("Edit User test error " + ex.getMessage());
		}
	}

	@Test(priority = 3, dataProvider = "add-employees")
	public void DeleteUser(String firstName, String lastName, String startDate, String email) {
		try {
			// select user
			// lastName and firstName are switched in Edit Testcase
			employeesPage.selectEmployee(lastName + " " + firstName);
			Thread.sleep(500);
			employeesPage.pressDeleteButton();
			Thread.sleep(500);
			employeesPage.employeeDeleteConfirmation(true);
			Thread.sleep(500);
			Assert.assertFalse(employeesPage.employeeExists(lastName + " " + firstName),
					"User [" + lastName + " " + firstName + "] was still found after deletion");
		} catch (Exception ex) {
			System.out.println("Delete User test error " + ex.getMessage());
		}
	}

	@Test(priority = 4)
	public void Logout() {
		clearTestData();
		employeesPage.pressLogoutButton();
	}

	@DataProvider(name = "add-employees")
	public Object[][] dataProviderAddEmployees() {
		return new Object[][] { { "Anakin", "Skywalker", "2004-01-01", "anakin.skywalker@tatooine.com" },
				{ "Mace", "Windu", "2005-02-02", "mace.windu@naboo.com" },
				{ "Obi Wan", "Kenobi", "2006-03-03", "obiwan.kenobi@coruscant.com" } };
	}

	private String changeDate(String date) {
		return date;
	}

	private String changeEmail(String email) {
		return email.replace("com", "org");
	}

	private void clearTestData() {
		for (Object[] item : dataProviderAddEmployees()) {
			try {
				if (employeesPage.selectEmployee(item[0] + " " + item[1])) {
					Thread.sleep(50);
					employeesPage.pressDeleteButton();
					Thread.sleep(100);
					employeesPage.employeeDeleteConfirmation(true);
					Thread.sleep(100);
				}
				if (employeesPage.selectEmployee(item[1] + " " + item[0])) {
					Thread.sleep(50);
					employeesPage.pressDeleteButton();
					Thread.sleep(100);
					employeesPage.employeeDeleteConfirmation(true);
					Thread.sleep(100);
				}
			} catch (Exception ex) {

			}
		}
	}
}
