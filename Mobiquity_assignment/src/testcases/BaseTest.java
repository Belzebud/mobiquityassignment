package testcases;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import pages.CreatePage;
import pages.EditPage;
import pages.EmployeesPage;
import pages.LoginPage;

public class BaseTest {
	protected WebDriver driver;
	protected LoginPage loginPage;
	protected EmployeesPage employeesPage;
	protected EditPage editPage;
	protected CreatePage createPage;

	private final String webPage = "http://cafetownsend-angular-rails.herokuapp.com";
	protected final String userName = "Luke";
	protected final String password = "Skywalker";

	@BeforeTest
	protected void preConditions() {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		loginPage = new LoginPage(driver);
		employeesPage = new EmployeesPage(driver);
		editPage = new EditPage(driver);
		createPage = new CreatePage(driver);
	}

	@AfterTest
	protected void postConditions() {
		driver.manage().deleteAllCookies();
		driver.close();
	}

	protected void goToWebSite() {
		driver.get(webPage);
		driver.manage().window().maximize();
	}
}
